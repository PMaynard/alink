# Alink

This is a simple book mark service built with ada.

	sudo apt install make gnat libaws3.3.2.2-dev
	make
	./main

It accepts URIs via HTTP Post:

	curl -F 'url=https://duckduckgo.com/' http://localhost:8080
	curl -F 'a=b' http://localhost:8080
	curl -X POST http://localhost:8080

