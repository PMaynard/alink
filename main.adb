with Ada.Text_IO;

with AWS.Default;
with AWS.Server;
with AWS.Server.Log;

with GET_CB;

procedure main is

   WS : AWS.Server.HTTP;

begin

   Ada.Text_IO.Put_Line
     ("Call me on port"
      & Positive'Image (AWS.Default.Server_Port)
      & ", press 'q' to stop server. Logs are in 'main.log'.");


   AWS.Server.Start (WS, "Alink",
                     Max_Connection => 1,
                     Callback       => GET_CB.GET_CB'Access);

   -- TODO: Use Method dispatcher

   -- Enable Logging.
   AWS.Server.Log.Start (WS);

   --  Wait for 'q' key pressed...
   AWS.Server.Wait (AWS.Server.Q_Key_Pressed);

   --  Close servers.
   AWS.Server.Shutdown (WS);
end main;
