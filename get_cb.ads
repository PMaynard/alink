with AWS.Response;
with AWS.Status;

package GET_CB is

   function GET_CB (Request : AWS.Status.Data) return AWS.Response.Data;
   function SHOW_URLS (Request : AWS.Status.Data) return AWS.Response.Data;
   function ADD_URL (Request : AWS.Status.Data) return AWS.Response.Data;

end GET_CB;
