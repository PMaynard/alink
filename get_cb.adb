with Ada.Text_IO;
-- with Ada.Strings.Unbounded; 
with Ada.Streams.Stream_IO;

with AWS.Messages;
with AWS.MIME;

package body GET_CB is

   DB_FILE : String := "data/urls.txt"; -- TODO: Move this to main.adb

   --------------------
   -- Main Call back --
   --------------------
   function GET_CB (Request : AWS.Status.Data)
      return AWS.Response.Data
   is
      URI : constant String := AWS.Status.URI (Request);
      METHOD : constant String := AWS.Status.METHOD (Request);
   begin

      -- Return 404 for anything other than '/' 
      if URI /= "/" then
         return AWS.Response.Acknowledge (AWS.Messages.S404, "<p>Not found !");
      end if;

      -- If HTTP GET show links, if POST add link
      if METHOD = "GET" then
         return SHOW_URLS(Request);
      elsif METHOD = "POST" then
         return ADD_URL(Request);
      else
         return AWS.Response.Acknowledge (AWS.Messages.S404, "<p>Not found !");
      end if;

   end GET_CB;

   -----------
   -- SHOW_URLS
   -----------
   function SHOW_URLS (Request : AWS.Status.Data)
      return AWS.Response.Data
   is
   begin
      return AWS.Response.File (AWS.MIME.Text_Plain, DB_FILE);
   end;

   -----------
   -- ADD_URL 
   -----------
   function ADD_URL (Request : AWS.Status.Data)
      return AWS.Response.Data
   is
      -- RUN : constant Boolean := AWS.Status.Is_Body_Uploaded (Request);
      -- NEW_URL : constant Ada.Strings.Unbounded.Unbounded_String := AWS.Status.Binary_Data (Request);
      URL : String := "";
   begin

      if AWS.Status.Is_Body_Uploaded (Request) then
            -- Ada.Text_IO.Put_Line (Ada.Strings.Unbounded.To_String(NEW_URL));
            Ada.Streams.Write (Ada.Streams.Stream_IO.Stream (URL).all, AWS.Status.Binary_Data (Request));
            -- https://stackoverflow.com/questions/40044741/ada-serial-port-string-to-stream-element-array
            -- https://en.wikibooks.org/wiki/Ada_Programming/Libraries/Ada.Streams
            -- https://docs.adacore.com/aws-docs/aws/apiref.html#aws-status
            return AWS.Response.Build (AWS.MIME.Text_Plain, "Thanks!");
      else
         return AWS.Response.Acknowledge (AWS.Messages.S404, "<p>Not found !");
      end if;
   end ADD_URL;


end GET_CB;
